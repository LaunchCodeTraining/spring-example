# Lecture Notes

## Springboot

- extended Spring framework
- Spring Web libraries
- Apache Tomcat web server
- https://www.baeldung.com/spring-vs-spring-boot

We will learn more about some of the Spring features later in this class.

## Gradle

- build tool
- dependency manager
- performs tasks (like running our java projects, or creating build artifacts)

Gradle can be system wide, or run using a gradle wrapper which is an environment for a singular project.

### Gradle Files

At the root of the gradle project you should find these files:

- `build.gradle`
- `settings.gradle`

And if your project uses a gradle wrapper you should find the additional files:

- `gradle/wrapper/gradle-wrapper.properties`
- `gradlew`

### Working with a gradle wrapper

The `gradlew` file is a bash script that allows us to work with the built in gradle tasks.

You can view the available tasks by running: `./gradlew tasks` from your terminal at the project root.

We will be running our Springboot projects with the gradle task `bootRun`. So you will be able to run a Springboot project by running `./gradlew bootRun` in your terminal at the project root.

## Spring Web Basics

The general objective of the Spring Web module is to give the ability to map HTTP requests to a Java method and to return an HTTP response from that method.

As the developer of the API we decide which paths and HTTP methods can be made. We are also responsible for providing the logic necessary to return an HTTP response.

What this will look is we will define a Controller class in Java. We will bind the controller class to a specific path, and then we will add methods to this class for each HTTP method and path associated with this Controller class.

### Java Annotations

- start with `@`
- represents meta data, most commonly configuration data
- are used heavily within Spring
- will allow us to quickly, and easily add additional data to various classes, methods, and variables

### Spring Web Annotations

- `@RestController`: defines a controller class as a RestController
- `@RequestMapping`: defines the mapping of a Controller class, or HTTP handler method
- `@GetMapping`: defines a method as an HTTP GET handler method
- `@PostMapping`: defines a method as an HTTP POST handler method
- `@PathVariable`: bound to an argument of a HTTP handler method to indicate an HTTP path variable
- `@RequestParam`: bound to a query parameter of an incoming HTTP request
- `@RequestBody`: used with an HTTP request handler argument to indicate incoming JSON data

We will be using these annotations in combination with Java to create fully customized web APIs.