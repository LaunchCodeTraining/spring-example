package org.launchcode.springexample.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class IndexController {

    // GET http://localhost:8080/
    @GetMapping
    public String getIndex() {
        return "hello from spring";
    }

    @PostMapping
    public String postIndex() {
        return "you made a POST request to /";
    }

    // // GET http://localhost:8080/goodbye
    // @GetMapping(value = "goodbye")
    // public String getGoodbye() {
    //     return "goodbye";
    // }
    
}
