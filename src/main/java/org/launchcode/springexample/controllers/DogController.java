package org.launchcode.springexample.controllers;

import org.launchcode.springexample.models.Dog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/dog")
public class DogController {

    Dog newDog = new Dog("Bernie", "Basset", 4);

    @GetMapping
    public Dog getDog() {
        return newDog;
    }

    @PostMapping
    public Dog postDog(@RequestBody Dog incomingDog) {
        newDog = incomingDog;
        return newDog;
    }

    // http://localhost:8080/dog/15

    @GetMapping(value = "/{id}")
    public Dog getDogById(@PathVariable int id) {
        System.out.println(id);
        // some form of database lookup using the id variable they passed in with the path variable
        return new Dog("Samson", "Terrier", 7);
    }
    
}
