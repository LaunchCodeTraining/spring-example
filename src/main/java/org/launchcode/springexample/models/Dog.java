package org.launchcode.springexample.models;

public class Dog {

    // properties

    private String name;
    private String breed;
    private int age;

    // constructor

    public Dog(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }

    // getters() & setters()

    public String getName() {
        return this.name;
    }

    public String getBreed() {
        return this.breed;
    }

    public int getAge() {
        return this.age;
    }
    
}
